# Otter Grading Service Setup

This role installs a grading instance for the *otter grading service*, and optionally the RabbitMQ queueing service.

Note that you may install more than one grading instance, but only one queueing system.

## Requirements

The host system must support Docker in daemon mode! The Unix socket controlling Docker will
be mounted into the grader service.

Free disk space depends on the image used for grading, which can be large. 60 GB recommended.

## Configuration
### General
The Ansible variables recognized are (defaults in brackets):

- `local_rabbitmq` (true/**false**): install a Docker based RabbitMQ instance
- `grader_service_version` (">=0.0.32"): Python version specifier, example: "==1.0.3"

### Grading Settings
These variables configure the grading service. They correspond to the [environment variables
used by the grader](https://gitlab.ethz.ch/k8s-let/grading-service/otter-grader-service).

- `grader_loglevel` (INFO)
- `grader_deployment_type` (prod)
- `grader_cert_directory` (/etc/rabbitmq/certs)
- `grader_rabbitmq_host` ("")
- `grader_rabbitmq_virtualhost` ("")
- `grader_rabbitmq_user` ("")
- `grader_rabbitmq_password` ("")
- `grader_docker_cmd` (docker)
- `grader_grading_home` (/home/jovyan)
- `grader_grading_image` (registry.ethz.ch/k8s-let/notebooks/jh-notebook-universal-ethz:3.0.0-02)

If you leave `grader_rabbitmq_virtualhost` and `grader_rabbitmq_user` untouched, they will be set
automatically following a pattern:

- `grader_rabbitmq_virtualhost` becomes `ottergrader-<grader_deployment_type>`
- `grader_rabbitmq_user` becomes `otter-<grader_deployment_type>`

### RabbitMQ TLS
TLS is required. The files contents have to be supplied as variables, the key files
preferably encrypted (ie. with `ansible-vault`). All variables default to an empty string.

- `rabbitmq_ca_content`
- `rabbitmq_client_cert_content`
- `rabbitmq_client_key_content`

If RabbitMQ is managed by this Ansible role, also set these variables:

- `rabbitmq_server_cert_content`
- `rabbitmq_server_key_content`
