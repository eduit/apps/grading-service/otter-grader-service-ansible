# Ansible Collection - ethz.otter_grader_service_testing

Maintain installations of [otter-grader-service](https://gitlab.ethz.ch/k8s-let/grading-service/otter-grader-service).

## Testing Installation

The role [testserver](roles/testserver/README.md) can be used to install a testing 
environment. It grades different submissions and provides a service which reports
the result of the grading runs.

## Production Installation
